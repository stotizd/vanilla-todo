

//КОД ПО ПРИКОЛУ НАПИСАН НА РУССКОМ, ТИПА 1С, РОССИЯ И Т.П
//КОД ПО ПРИКОЛУ НАПИСАН НА РУССКОМ, ТИПА 1С, РОССИЯ И Т.П


const
    тело = document.querySelector('body'),
    список = document.querySelector('.списокЗаписей'),
    вход = document.querySelector('.вход'),
    количество = document.querySelector('#количество'),
    голос = document.querySelector('audio'),
    кнопка = document.querySelector('.учет'),
    удачные = document.querySelector('#удачные');

let базаДанных = [
    {отметка: false, ФИО: 'Гунбергалымы Мухмобермедов', номер: 1}
];


тело.addEventListener('click', (событие) => {общаяФункция(событие)});
тело.addEventListener('keypress', (событие) => {общаяФункция(событие)});
window.addEventListener('touchstart', () => {кнопка.style.display = 'block'});
кнопка.addEventListener('click', (событие) => {общаяФункция(событие)});
window.addEventListener('DOMContentLoaded', (событие) => {общаяФункция(событие)});

if(localStorage.getItem('датаЦентр')){
    базаДанных = JSON.parse(localStorage.getItem('датаЦентр'));
    
}

function общаяФункция(событие) {
    if(событие.code == 'Enter' || событие.pointerId && вход.value !== ''){
        базаДанных = [...базаДанных,{отметка: false, ФИО: вход.value, номер: Date.now()}]
        вход.value = '';
    }

    отрисовка()
    const 
        доебаться = document.querySelectorAll('.доебаться'),
        записи = document.querySelectorAll('.записи'),
        депортация = document.querySelectorAll('.депортация');

    доебаться.forEach((значение) => {
        значение.addEventListener('click', (н) => {
            базаДанных.forEach((сущ) => {
                if(сущ.номер == значение.parentElement.parentElement.id && сущ.отметка == false){
                    сущ.отметка = true;
                    голос.load();
                    голос.play();
                }
            })
        })
    });

    депортация.forEach((значение, н) => {
        значение.addEventListener('click', () => {
            базаДанных.splice(н, 1);
            отрисовка()
            
        })
    });
    localStorage.setItem('датаЦентр', JSON.stringify(базаДанных));
    подсчет();
    отметка(записи);
}

function отметка(записи) {
    базаДанных.forEach((единица) => {
        if(единица.отметка){
            записи.forEach((e) => {
                if(e.id == единица.номер){
                    e.classList.add('отмечено')
                }
            });
        }
    })
}

function отрисовка() {
    let готовыеСписки = '';
    if(базаДанных.length == 0){
        список.innerHTML = ''
    }
    базаДанных.forEach((единица) => {
        готовыеСписки += `
            <div class="записи" id="${единица.номер}">
                <div class="ФИО">${единица.ФИО}</div>
                <div class="команды">
                    <button class="доебаться">Доебаться</button>
                    <button class="депортация">Депортировать</button>
                </div>
            </div>
        `;
        список.innerHTML = готовыеСписки;
    })
    подсчет()
}
отрисовка()

function подсчет() {
    const 
        сумма = базаДанных.length,
        суммаВыполненых = базаДанных.filter(сущ => сущ.отметка == true).length;
    количество.textContent = ` ${сумма}`;
    удачные.textContent = ` ${суммаВыполненых}`;
}









